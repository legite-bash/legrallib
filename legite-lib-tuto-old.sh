#!/bin/bash

set -u
#set -eE  # same as: `set -o errexit -o errtrace`

declare -r SCRIPT_PATH=$(realpath $0);        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=$(dirname $SCRIPT_PATH);# repertoire absolu du script (pas de slash de fin)
declare -r VERSION='2021.11.29';

if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh
isDebug=1;

declare -r TEST_REP='/dev/shm/tutos'






#####################
## APPEL DES TESTS ##
#####################
function _tutos(){
    fctIn "$FUNCNAME($*)"



    fctOut
    return 0
}

###################
## Les variables ##
###################

titre1 "Les variables bash"$NORMAL
echo "\$0:Contient le nom du script tel qu'il a été invoqué: $0"
echo "\$*: L'ensembles des paramètres sous la forme d'un seul argument: $*"
echo "\$@: L'ensemble des arguments, un argument par paramètre: $@"
echo "\$#: Le nombre de paramètres passés au script: $#"
echo "\$?: Le code retour de la dernière commande: $?"
echo "\$$: Le PID su shell qui exécute le script: $$"
echo "\$\!: Le PID du dernier processus lancé en arrière-plan: ${!:-'aucun'}"
displayVar '$FUNCNAME' "$FUNCNAME"


titre2 'Declaration de constantes'
    evalCmd "declare -r constanteNonType='variable de test'"
    evalCmd "readonly varEnLectureSeul=10"

titre2 'Declaration d une variable non typé'
    evalCmd "declare varNonType"

titre2 'Declaration d une [constante] variable integer'
    evalCmd "declare [-r] -i varInt"


titre2 "Declaration d'un tableau"
    evalCmd "declare -a tableauOrdonnee"

titre2 "Declaration d'un tableau associatif"
    evalCmd "declare -A tableauAssociatif"


titre2 "Initialiser une variable avec une autre ou une valeur par défaut si vide"
    evalCmd "declare varExistant='varExist'";
    evalCmd "heriteOuDef=${varExistant:-'valeurParDefaut'}"
    displayVar 'heriteOuDef' "$heriteOuDef"

titre2 'Vérifier si une variable est vide ou non'
    echo 'if [ -z ${varATester+inconnu} ]'
    if [ -z ${varATester+inconnu} ]
    then echo "var is unset";
    else echo "var is set to '$var'"
    fi

titre1 'Comparaisons'

titre2 "Chaine egales"
    a="aaa";
    if [ "$a" == "$a" ]
    then
        echo "equivalence avec '=='"
    fi

    if [ "$a" = "$a" ]
    then
        echo "equivalence avec '='"
    fi


titre1 'operateur ternaire'
    evalCmd '[ "$(id -u)" == "0" ] && isRoot=1 || isRoot=0'


titre2 'Vérifier si un parametre est défini';
    echo 'if [ -z ${2+x} ]'
    if [ -z ${2+x} ]
    then echo "parametre 2 is unset";
    else echo "parametre 2 is set to '$2'";
    fi

titre2  'chaine vide';
    evalCmd "chaineVide='';"
    echo 'if [ "$chaineVide" == "" ]'
    if [ "$chaineVide" == "" ];
    then echo "$chaineVide est vide";
    else echo "$chaineVide est PAS vide";
    fi


###################
## Les variables ##
###################
titre1 '(try/catch)'
echo 'https://stackoverflow.com/questions/22009364/is-there-a-try-catch-command-in-bash'


##################
## Les tableaux ##
##################
titre1 'tableaux de fonctions'
    echo "declare -a pour un tableau indicé"
    echo "declare -A pour un tableau associatif"

declare -A fonctionsTbl;

titre2 'Une fonction dans un tableau'
function fonction1(){echo 'Ceci est la fonction1'}
evalCmd "fonctionsTbl['fct1']=fonction1";
evalCmd ${fonctionsTbl['fct1']};


titre2 'Manipulation de tableaux'
    #evalCmd "declare -A tableauTest;"
    declare -A tableauTest;
    evalCmd 'tableauTest["defini"]="ok";'

    titre2 "# - echo sur un champ defini - #"
    echo '${tableauTest["defini"]}='${tableauTest["defini"]};

    titre2 "# - echo sur un champ NON defini - #"
    echo '${tableauTest["NonDefini"]};'
    echo "Le programme s'arrete si set -u"

    titre2 "# - Tester si champ defini - #"
    echo 'if [ ${tableauTest["defini"]+x} ]'
    if [ ${tableauTest["defini"]+x} ]
    then
        echo "Champ défini";
    else
        echo "Champ NON défini";
    fi


titre1 'Declaration d un tableau pour les tests suivants'
    echo "declare -r -a a=('un' 'deux' 'trois' 'quatre et cing')";
    declare -r -a a=('un' 'deux' 'trois' 'quatre et cing')

    titre2 "Nombre d'element d'un tableau"
    echo '${#a[@]}='${#a[@]}

    titre2 'Lecture de tableau'
    echo '${a[*]}='${a[*]}
    echo 'for valeur in ${a[*]}'
        for valeur in ${a[*]}
    do
        displayVar "$valeur" "$valeur"
    done

    echo ''
    echo '${a[@]}='${a[@]}
    echo 'for valeur in "${a[@]}"'
        for valeur in "${a[@]}"
    do
        displayVar "$valeur" "$valeur"
    done

    echo ''
    echo 'for ((index=0; index < ${#a[@]}; ++index))'
        for ((index=0; index < ${#a[@]}; ++index))
    do
        displayVar "${a['$index']}" "${a[$index]}"
    done


titre1 "Portabilité d'une variable local dans une sous fonction"


UneVarNonLocaleExisteElleApresLaFonction(){
    echo "Une var creer dans une fonction mais declaré non local existe t elle encore apres la fonction?->NON. Une fonction declarer dans une fonction est defacto local"
    varNonLocale="local"
    displayVar "varNonLocale" "$varNonLocale"
}
#echo "varLocal=$varNonLocale" #-> non declarer


echo ""
varLocal="global"
createVarLocal(){
    displayVar "varLocal" "$varLocal"
    local varLocal="local"
    displayVar "varLocal" "$varLocal"
    callVarLoval
    displayVar "varLocal" "$varLocal"
    declare variableDeclarerDansFonction=1
    variableCreeNonDeclarerDansFonction=1
}
callVarLoval(){
    varLocal="sous fonction"
}

createVarLocal;
echo "varLocal=$varLocal"
echo "variableDeclarerDansFonction=${INFO}variable sans liaison$NORMAL"
echo "variableCreeNonDeclarerDansFonction=$variableCreeNonDeclarerDansFonction"


echo ""
echo $LINENO:"#################################"
echo $TITRE1"type"$NORMAL
test_type(){
    test_fonction(){
        echo "ceci est une fonction"
    }
    # https://bash.cyberciti.biz/guide/Type_command
    #type varA  # ne fonctionne pas avec les variables
    #echo "type test_fonction"=$(type test_fonction)
    type -t test_fonction
}
test_type

fonctionExist(){
    echo $LINENO 'oui'
}

if [ $(type -t fonctionExist) == 'function' ]
then
    echo $LINENO "oui"
fi


echo ""
echo $LINENO:"#################################"



echo ""
echo $LINENO:"#################################"
echo $TITRE1"isNumeric"$NORMAL

isNumeric(){
    local -i numeric=$1
    local errNu=$?
    displayVar "errNu" "$errNu" "numeric" "$numeric"
}

testIsNumeric(){
    isNumeric 1000
    isNumeric "1001"
    isNumeric "NoInteger"
}
#testIsNumeric




#################
## TEST PARAMS ##
#################
    test_AfficherParam(){
        echo "$FUNCNAME($*)"

        fTxt1(){
            echo ''
            echo "$FUNCNAME($*)"
            showDebug "$FUNCNAME($*)"

            local p1="${1:-''}";displayVar "$FUNCNAME-p1" "$p1";
            local p2="${2:-''}";displayVar "$FUNCNAME-p2" "$p2";
            local p3="${3:-''}";displayVar "$FUNCNAME-p3" "$p3";
            local p4="${4:-''}";displayVar "$FUNCNAME-p4" "$p4";
            
            fTxt2 "$p1" "$p2" "$p3" "$p4"

        }

        fTxt2(){
            echo "$FUNCNAME($*)"
            echo "$FUNCNAME($*)"
            showDebug "$FUNCNAME($*)"
            showDebug "$FUNCNAME($*)"
            local p1="${1:-''}";displayVar "$FUNCNAME-p1" "$p1";
            local p2="${2:-''}";displayVar "$FUNCNAME-p2" "$p2";
            local p3="${3:-''}";displayVar "$FUNCNAME-p3" "$p3";
            local p4="${4:-''}";displayVar "$FUNCNAME-p4" "$p4";
        }

        isDebug=1;
        fTxt1 "TXT1" 'TXT A' "TXT B"
        fTxt1 "TXT1" 10 'TXT A' "TXT B"
        fTxt1 "TXT1" 10 'TXT A' 20

    }
#


##################
## TEST SYNTAXE ##
##################

    function test_syntaxes(){
        fctIn "$FUNCNAME($*)"        

        # --------- #
        argumentDeFonctions 1 2 'trois pas tout seul' "quatre accompagné"

        # --------- #
        local names=( 'Anderson da Silva' 'Wayne Gretzky' 'David Beckham' 'Long
        Name');

        test_syntaxes_ArobaseEtoile

        # --------- #
        test_syntaxe_FonctionArobaseEtoile un deux 'trois pas     tout seul' "quatre contenant un 
            retour chariot"
        fctOut
    }


    function argumentDeFonctions(){
        fctIn "$FUNCNAME($*)"
        echo 'for "'
        local _argNb=0;
        while true
        do
            ((_argNb++))
            p=${1:-''}
            if [ "$p" == '' ];then break;fi
            displayVar "$_argNb" "$1"; shift
        fctOut
        done
    }

    function test_syntaxes_ArobaseEtoile(){
        fctIn "$FUNCNAME($*)"
        function showDataArobase(){
            fctIn "$FUNCNAME($*)"
            for name in ${names[@]};  do displayVar 'names[@]' "$name"; done
        }
        function showDataDollarArobase(){
            fctIn "$FUNCNAME($*)"
            for name in ${names[$@]};  do displayVar 'names[$@]' "$name"; done
        }
        function showDataEtoile(){
            fctIn "$FUNCNAME($*)"
            for name in ${names[*]};  do displayVar 'names[*]' "$name"; done
        }
        function showDataDollarEtoile(){
            fctIn "$FUNCNAME($*)"
            for name in ${names[$*]};  do displayVar 'names[$*]' "$name"; done
        }


        echo "Quand c'est un tableau:"
        echo "Il n'y a aucune difference entre \$@ et \$*: les 2 renvoient le 1er indice"
        echo "Il n'y a aucune difference entre @ et $*: les 2 renvoient TOUS les indices"
        echo "Avec l'IFS avec espace: chaque mot devient un indice"

        echo -e "\nWith default IFS value..."
        IFS=$' \n\t'  # ' \n\t' is the default IFS value
        #IFS=$' \n\t'

        showDataArobase
        showDataEtoile
        showDataDollarArobase
        showDataDollarEtoile

        echo -e "With strict-mode IFS value..."
        IFS=$'\n\t'
        showDataArobase
        showDataEtoile
        showDataDollarArobase
        showDataDollarEtoile
        fctOut
    }

    function test_syntaxe_FonctionArobaseEtoile(){
        fctIn "$FUNCNAME($*)"        
        echo "Quand ce sont les parametres d'une fonction:"
        echo "Il n'y a aucune difference entre \$@ et \$*: les 2 renvoient le 1er indice"
        echo "Il n'y a aucune difference entre  @ et  *: les 2 renvoient TOUS les indices"
        echo "Avec l'IFS avec espace: chaque mot devient un indice"

        #echo '@ est le caractere @'
        #for parametre in  @; do displayVar 'parametre[@] ' "$parametre"; done
        echo '$@ est le tableau de parametres'
        for parametre in $@; do displayVar 'parametre[$@]' "$parametre"; done
        echo ''

        #echo '* est le repertoire courant'
        #for parametre in  *; do displayVar 'parametre[*] ' "$parametre"; done
        echo '$* est le tableau de parametres'
        for parametre in $*; do displayVar 'parametre[$*]' "$parametre"; done
        fctOut
    }
#


##############
## TEST FOR ##
##############
    # --------- #
    function test_for(){
        fctIn "$FUNCNAME($*)"

        test_for_avec_tableau
        test_for_avec_variable
        fctOut
    }

    function test_for_avec_tableau(){
        fctIn "$FUNCNAME($*)"
        echo 'Tbleau associatif'
        local books=('In Search of Lost Time' 'Don Quixote' 'Ulysses' 'The Great Gatsby')
        echo '@ ';
        for book in "${books[@]}";  do  displayVar 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}"; do  displayVar 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";  do  displayVar 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}"; do  displayVar 'Book' "$book";done

        echo 'Tableau indexé'
        declare -a books;
        books[0]='In Search of Lost Time';
        books[1]='Don Quixote';
        books[2]='Ulysses';
        books[3]='The Great Gatsby';
        echo '@ ';
        for book in "${books[@]}";  do  displayVar 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}"; do  displayVar 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";  do  displayVar 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}"; do  displayVar 'Book' "$book";done
        fctOut
    }

    function test_for_avec_variable(){
        fctIn "$FUNCNAME($*)"
        #for val in {1..${end}}  do  displayVar 'val' "$val";done
        #ne fonctionne pas
        fctOut
    }
#


########################
## TEST SUBSTITUTIONS ##
########################
    # --------- #
    # https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
    function test_substitutions(){
        fctIn "$FUNCNAME($*)"

        test_substitution_rename
        #test_substitution_
        fctOut
    }

    function test_substitution_rename(){
        fctIn "$FUNCNAME($*)"
        local original='';
        original='fichier.jpeg';   displayVar "$original" "${original%.jpeg}.jpg"
        original=' aA bB cC';       displayVar "$original" "${original// /_}"
        original='_aAbBcC';         displayVar "$original" "${original//A/}"
        fctOut
    }


#


################
## TEST SPLIT ##
################
    test_splitHHMMSS(){
        fctIn "$FUNCNAME($*)"

        splitHHMMSS
        splitHHMMSS '0'
        splitHHMMSS 5
        splitHHMMSS 45

        splitHHMMSS '8:8'
        splitHHMMSS '0:1:6'

        splitHHMMSS '18:8:6'
        fctOut
    }
#


################
## TEST TRAPS ##
################
    test_traps(){
        fctIn "$FUNCNAME($*)"
        
        #echo 'test avec un return 1';   return 1;
        fail 1;displayVar '$?' "$?"        
        fail 2;displayVar '$?' "$?"
        fail 3;displayVar '$?' "$?"
        fctOut
        return 10;
    }


################
## PARAMETRES ##
################
TEMP=$(getopt \
    --options v::dVhnS \
    --long help,version,verbose::,debug,showVars,showCollections,update,conf::,showLibs,showDuree\
,none\
    -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"
        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                shift ;;
            -V|--version) echo "$VERSION"; exit 0;      shift ;;
            -v|--verbose)
                case "$parametre" in
                    '') verbosity=1; shift 2 ;;
                    *)  #echo "Option c, argument '$2'" ;
                    verbosity=$parametre; shift 2;;
                esac
                ;;
            -d|--debug) isDebug=1;isShowVars=1;         shift  ;;
            --update)   isUpdate=1;                     shift ;;

            --showVars)         isShowVars=1;           shift  ;;
            --showCollections)  isShowCollections=1;    shift  ;;
            --showLibs )        isShowLibs=1;           shift  ;;
            --showDuree )       isShowDuree=1;          shift  ;;

            --conf)     CONF_PSP_PATH="$parametre";  shift 2; ;;

            -S)
                isSimulation=1;
                YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
                shift
                ;;

            --collection)       collection=$parametre; shift 2 ;;

            #--) # parcours des arguments supplementaires
            --)
                if [ -z "$parametre" ]
                then
                    shift;
                    break;
                fi
                #echo "--)$argument,$parametre"
                librairie="$parametre";
                librairiesCall[$librairiesCallIndex]="$librairie"
                ((librairiesCallIndex++))
                shift 2;
                ;;

            *)
                if [ -z "$argument" ]
                then
                    shift 1;
                    break;
                fi
                #echo "*)$argument,$parametre"
                librairie="$argument";
                librairiesCall[$librairiesCallIndex]="$librairie"
                ((librairiesCallIndex++))
                shift 1;
                ;;

            esac
    done
#


#######################
# LANCEMENT DES TESTS #
#######################
clear
# creation de l'espace de test
mkdir -p $TEST_REP
cd $TEST_REP
if [ $? -ne 0 ];then exit 1;fi

_tutos
