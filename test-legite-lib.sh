#!/bin/bash

# Fichier de test de la librairie legite-liub.sh

set -u
#set -eE  # same as: `set -o errexit -o errtrace`

# SYNTAXE D'apelle:
# ./test-legite-lib.sh -V
# ./test-legite-lib.sh -d 
# ./test-legite-lib.sh void
# ./test-legite-lib.sh collection lib
# ./test-legite-lib.sh test_colorisations
# ./test-legite-lib.sh
# ./test-legite-lib.sh


declare -r SCRIPT_PATH=$(realpath $0);        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=$(dirname $SCRIPT_PATH);# repertoire absolu du script (pas de slash de fin)
declare -r VERSION='2022.04.30';

if [[ ! -f "$SCRIPT_REP/legite-lib.sh" ]]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh



# Activation des modes de debug
#isDebug=1;
#isShowVars=1
isShowLibs=1;
declare -r TEST_REP='/dev/shm/tests'


#declare -a tbl_test=('UN' 'DEUX' 'TROIS' 'QUATRE' )
#isInArray  "${tbl_test[@]}" 'DEUX'

    function showVarsLocal() {
        fctIn
        fctOut "$FUNCNAME"; return 0;
    }


#####################
## APPEL DES TESTS ##
#####################
function call_tests(){
    fctIn;

    #test_colorisations
    #test_AfficherParam;
    #test_PSP_supp

    #test_traps
    #echo "valeur de retour du test de test_traps(): $?"
    #test_trapC

    #test_debug
    #test_echoV
    #test_funcname
    #test_exec
    #test_require

    #time test_getNormalizeText
    #time test_echoNormalizeText
    #test_splitHHMMSS

    #test_librairies


    fctOut "$FUNCNAME";return 0;
}

#######################
## TEST COLORISATION ##
#######################
    addLibrairie 'test_colorisations'
        function test_colorisations(){
        fctIn

        titre1 'titre1'
        titre2 'titre2'
        titre3 'titre3'
        titre4 'titre4'
        
        fctOut "$FUNCNAME";return 0;
    }
#


#################
## TEST PARAMS ##
#################
    addLibrairie 'test_AfficherParam'
    function test_AfficherParam(){
        fctIn "$*"

        function fTxt1(){
            fctIn "$*"

            echo  '$@' "$@"
            displayVar '$*' "$*"
            local p1="${1:-''}";displayVar "$FUNCNAME($@)-p1" "$p1";
            local p2="${2:-''}";displayVar "$FUNCNAME($@)-p2" "$p2";
            local p3="${3:-''}";displayVar "$FUNCNAME($@)-p3" "$p3";
            local p4="${4:-''}";displayVar "$FUNCNAME($@)-p4" "$p4";
            
            fTxt2 "$p1" "$p2" "$p3" "$p4"
            fctOut "$FUNCNAME";return 0;
        }

        function fTxt2(){
            fctIn "$*"
            local p1="${1:-''}";displayVar "$FUNCNAME($@)-p1" "$p1";
            local p2="${2:-''}";displayVar "$FUNCNAME($@)-p2" "$p2";
            local p3="${3:-''}";displayVar "$FUNCNAME($@)-p3" "$p3";
            local p4="${4:-''}";displayVar "$FUNCNAME($@)-p4" "$p4";
            fctOut "$FUNCNAME";return 0;
        }

        isDebug=1;
        evalCmd 'fTxt1 "TXT1" "TXT A" "TXT B"'
        evalCmd 'fTxt1 "TXT1" 10 "TXT A" "TXT B"'
        evalCmd 'fTxt1 "TXT1" 10 "TXT A" 20'
        fctOut "$FUNCNAME";return 0;

    }

    function test_PSP_supp(){
        fctIn "$*"
        fctOut "$FUNCNAME";return 0;
    }

#


################
## TEST TRAPS ##
################
    addLibrairie 'test_traps'
    function test_traps(){
        fctIn "$*"
        #fctOut "$FUNCNAME";return 1;
        #fctOut "$FUNCNAME";return $E_INTERNE;
        #fctOut "$FUNCNAME";return $E_ARG_REQUIRE;
        fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        #fctOut "$FUNCNAME";return $E_NORMAL;
        #fctOut "$FUNCNAME";return $E_ARG_NONE;
        #fctOut "$FUNCNAME";return $E_ARG_BAD;
        test_trapC
        fctOut "$FUNCNAME";return 0;
    }

    addLibrairie 'test_trapC'
    function test_trapC(){
        fctIn "$*"    
        while true
        do
            if [[ $isTrapCAsk -eq 1 ]];then break;fi
        done

        fctOut "$FUNCNAME";return 0;
    }
#


######################
# TEST NOTIFICATIONS #
######################
#


################
# TEST ERREURS #
################
#


#########
# DEBUG #
#########
    addLibrairie 'test_debug'
    function test_debug(){
        fctIn "$*"    
        displayVarNotNull 'chaine vide' ''
        displayVarDebug 'Sera Afficher uniquement en mode debug' "debug=$isDebug"
        displayVarDebug 'param1' "parametre 1" 'param2' "parametre 2"
        fctOut "$FUNCNAME";return 0;
    }
#


#########
# ECHOV #
#########
    addLibrairie 'test_echoV'
    function test_echoV(){
        fctIn "$*"
        local _d1='';local d2='';
        vic=0;_d1=$(displayVar 'vic' "$vic" 'verbosity' "$verbosity");_d2=$(echoV 'valeur ok'); echo "$_d1:$_d2";
        vic=1;_d1=$(displayVar 'vic' "$vic" 'verbosity' "$verbosity");_d2=$(echoV 'valeur ok'); echo "$_d1:$_d2";
        vic=2;_d1=$(displayVar 'vic' "$vic" 'verbosity' "$verbosity");_d2=$(echoV 'valeur ok'); echo "$_d1:$_d2";
        vic=3;_d1=$(displayVar 'vic' "$vic" 'verbosity' "$verbosity");_d2=$(echoV 'valeur ok'); echo "$_d1:$_d2";
        fctOut "$FUNCNAME";return 0;
    }
#


# ############# #
# TEST FUNCNAME #
# ############# #
    addLibrairie 'test_funcname'
    function test_funcname(){
        fctIn "$*"
        function funcname1(){
            fctIn "$*"
            displayVar '${FUNCNAME[@]}' "${FUNCNAME[@]}"
            displayVar '$FUNCNAME' "$FUNCNAME"
            displayVar '${FUNCNAME[0]}' "${FUNCNAME[0]}"
            displayVar '${FUNCNAME[1]}' "${FUNCNAME[1]}"
            echo '$@' "$@"
            echo '$*' "$*"
            fctOut "$FUNCNAME";return 0;
        }
        function funcname2(){
            fctIn "$*"
            echo '$@' "$@"
            echo '$*' "$*"
            fctOut "$FUNCNAME";return 0;
        }

        funcname1 'F1P1' 'F1 P2'
        funcname2 'F2PA' 'F2 PB'


        fctOut "$FUNCNAME";return 0;
    }
#


# ################### #
# TEST EXEC / REQUIRE #
# ################### #
    addLibrairie 'test_exec'
    function test_exec(){
        fctIn
        execFile 'Fichier Inexistant'
        fctOut "$FUNCNAME";return 0;
    }


    addLibrairie 'test_require'
    function test_require(){
        fctIn
        #require 'Fichier avec exspace inexistant'
        require $SCRIPT_REP'/test-bash-include.sh';
        fctOut "$FUNCNAME";return 0;
    }
#


# ######## #
# SHOWVARS #
# ######## #


# #################### #
# FONCTION TABULATIONS #
# #################### #
#


################################
# TEST GESTION DES COLLECTIONS #
################################
#


###############################
# TEST GESTION DES LIBRAIRIES #
###############################
    
    # ## ajoute une librairie ##

    # forme minimal
    addLibrairie 'test_librairies';

    # forme nom + description
    addLibrairie 'fctDirect';

    # forme nom + description + code (inline)
    addLibrairie 'A_et_B' 'Execute la fonction A puis B' 'functionA;functionB'
    addLibrairie 'code inline' 'Execute la fonction A puis B' 'echo "$LINENO:$FUNCNAME: code inline"' # FUNCNAME='evamCmd'
    #addLibrairie 'code inline' 'Execute la fonction A puis B' "echo '$LINENO:$FUNCNAME: code inline'" # FUNCNAME existe pas

    function test_librairies(){
        fctIn
        execLib 'A_et_B'        # execute une liibrairie ajouté
        fctOut "$FUNCNAME";return 0;
    }

    function fctDirect() {
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*"
        if [[ $IS_ROOT -eq 0 ]];then echo $WARN"$FUNCNAME($@) doit être lancé en root!"$NORMAL;fctOut "$FUNCNAME"; return $E_NOT_ROOT; fi
        if [[ $# -ne 0 ]];      then erreursAddEcho "${BASH_SOURCE}:$LINENO:$FUNCNAME($@)( 'arg1' 'arg2' ) ($*) .Appellé par ${FUNCNAME[1]}()";       fctOut "$FUNCNAME"; return $E_ARG_REQUIRE;fi

        fctOut "$FUNCNAME"; return 0;
    }

    function functionA() {
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$BASH_SOURCE:$LINENO:$*"
        #if [[ $IS_ROOT -eq 0 ]];then echo $WARN"$FUNCNAME doit être lancé en root!"$NORMAL;fctOut "$FUNCNAME"; return $E_NOT_ROOT; fi
        #if [[ $# -ne 0 ]];      then erreursAddEcho "$FUNCNAME ( 'arg1' 'arg2'  ).";       fctOut "$FUNCNAME"; return $E_ARG_REQUIRE;fi

        echo "$BASH_SOURCE:$LINENO:$FUNCNAME($*)"
        fctOut "$FUNCNAME"; return 0;
    }

    function functionB() {
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$BASH_SOURCE:$LINENO:$*"
        #if [[ $IS_ROOT -eq 0 ]];then echo $WARN"$FUNCNAME doit être lancé en root!"$NORMAL;fctOut "$FUNCNAME"; return $E_NOT_ROOT; fi
        #if [[ $# -ne 0 ]];      then erreursAddEcho "$FUNCNAME ( 'arg1' 'arg2'  ).";       fctOut "$FUNCNAME"; return $E_ARG_REQUIRE;fi

        echo "$BASH_SOURCE:$LINENO:$FUNCNAME($*)"

        fctOut "$FUNCNAME"; return 0;
    }

#


#############################
# TEST MANIPULATION DE TEXT #
#############################
    addLibrairie 'test_dupliqueNCar'
    function test_dupliqueNCar(){
        fctIn "$*"
        evalCmd "dupliqueNCar"
        evalCmd "dupliqueNCar 0 '*'"
        evalCmd "dupliqueNCar 9 '*'"
        fctOut "$FUNCNAME";return 0;
    }

#######################
# TEST NORMALIZE TEXT #
#######################
    addLibrairie 'test_getNormalizeText'
    function test_getNormalizeText(){
        fctIn "$*"

        function displayNormalizeText(){
            if [[ $# -eq 0 ]]; then return 0; fi
            getNormalizeText "$1";
            displayVar "$1" "$normalizeText";
        }

        displayNormalizeText ""
        displayNormalizeText "ÉéàÇçÀ"
        displayNormalizeText "parenthèse(test)"
        displayNormalizeText "Une phrase normale."
        displayNormalizeText "Deux phrases normales. Font un paragraphe.Fin"
        displayNormalizeText "test car spéciaux: {}"
        displayNormalizeText "test car spéciaux: []"
        displayNormalizeText "test car spéciaux: &é\"'(-è_çà)="
        displayNormalizeText "@="
        displayNormalizeText "~#{[|\`\\^@]}"
        displayNormalizeText "---- a ____ b ---_--- c __-__ d _-_"
        displayNormalizeText "test du et & ca _&_"
        displayNormalizeText "Test d'extention: .mp4"
        displayNormalizeText "Test d'extention: a.b.c--.mp4"
        displayNormalizeText "Test d'extention: a.b.c__.mp4"
        displayNormalizeText "Test d'extention: a.b.c__-.mp4"
        displayNormalizeText "on #tag chez @ "
        displayNormalizeText "on #tag chez @moi"
        displayNormalizeText "Un undescore à la fin_"
        displayNormalizeText "Un espace à la fin "
        displayNormalizeText "Un tiret à la fin-"
        #displayNormalizeText ''
        fctOut "$FUNCNAME";return 0;
    }

    addLibrairie 'test_echoNormalizeText'
    function test_echoNormalizeText(){
        fctIn "$*"
        local _out='';
        #function displayNormalizeText(){
        #    if [[ $# -eq 0 ]]; then return 0; fi
        #    echoNormalizeText "$1";
        #    displayVar "$1" "$normalizeText";
        #}

        # laisser getNormalizeText pour initialiser normalizeText
        getNormalizeText ' La fonction echoNormalizeText() ne change pas pas la variable normalizeText (sous process)' 

        local _in='ÉéàÇçÀ';
        _out="$(echoNormalizeText "$_in")";displayVar '_in' "$_in" '_out' "$_out";
        _in='Une phrase normale.';
        _out="$(echoNormalizeText "$_in")";displayVar '_in' "$_in" '_out' "$_out";
        _in='Deux phrases normales. Font un paragraphe.Fin';
        _out="$(echoNormalizeText "$_in")";displayVar '_in' "$_in" '_out' "$_out";

        titre3 'Test des caracteres speciaux'
        _in='test car spéciaux: {}';
        _out="$(echoNormalizeText "$_in")";displayVar '_in' "$_in" '_out' "$_out";
        _in='test car spéciaux: 1? 2 ?';
        _out="$(echoNormalizeText "$_in")";displayVar '_in' "$_in" '_out' "$_out";
        _in='test car spéciaux: []';
        _out="$(echoNormalizeText "$_in")";displayVar '_in' "$_in" '_out' "$_out";

        _in="test car spéciaux: &é\"'(-è_çà)=";
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out";
        _in='@=';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out";
        _in='~#{[|\`\\^@]}';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out";
        _in='---- a ____ b ---_--- c __-__ d _-_';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out";
        _in='test du et & ca _&_';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out";
        _in="Test d'extention: .mp4";
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in="Test d'extention: a.b.c--.mp4";
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in="Test d'extention: a.b.c__.mp4";
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in="Test d'extention: a.b.c__-.mp4";
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in='on #tag chez @ ';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in='on #tag chez @moi';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in='Un undescore à la fin_';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"
        _in='Un espace à la fin ';
        _out=$(echoNormalizeText "$_in"); displayVar '_in' "$_in" '_out' "$_out"

        echo "normalizeText est inchangé"
        displayVar 'normalizeText' "$normalizeText"
        fctOut "$FUNCNAME";return 0;
    }
#




######################
# TEST FILE HORODATE #
######################

    # - splitHHMMSS - #
    addLibrairie 'test_splitHHMMSS'
    function test_splitHHMMSS(){
        fctIn "$*"

        evalCmd "splitHHMMSS"
        evalCmd "splitHHMMSS '0'"
        evalCmd "splitHHMMSS 5"
        evalCmd "splitHHMMSS 45"

        evalCmd "splitHHMMSS '8:8'"
        evalCmd "splitHHMMSS '0:1:6'"

        evalCmd "splitHHMMSS '18:8:6'"
        fctOut "$FUNCNAME";return 0;
    }
#


#####################
# TEST LEADING ZERO #
####################
#


###############
# TEST DIVERS #
###############

    addLibrairie 'test_showFreedisk'
    function test_showFreedisk(){
        fctIn "$*"

        mkdir -p "$TEST_REP/repertoire avec espace"
        #showFreedisk '/'
        #showFreedisk '/home'
        showFreedisk '/' 'rep inexistant' "$TEST_REP/repertoire avec espace" '/home'
        fctOut "$FUNCNAME";return 0;
    }
#


################
## PARAMETRES ##
################
TEMP=$(getopt \
    --options vdVhnS- \
    --long help,version,verbose,debug,showVars,showCollections,update,conf::,showLibs,showLibsDetail,showDuree\
,none\
    -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [[ $isTrapCAsk -eq 1 ]];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"

        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage;                        ;;
            -V|--version) echo "$VERSION"; exit 0;      ;;
            -v|--verbose) ((verbosity++));              ;;
            -d|--debug)   ((isDebug++));isShowVars=1;isShowDuree=1; ;;
            --update)           isUpdate=1;             ;;
            --showVars)         isShowVars=1;           ;;
            --showCollections)  isShowCollections=1;    ;;
            --showLibs )        ((isShowLibs++));       ;;
            --showLibsDetail)   isShowLibs=2;           ;;
            --showDuree )       isShowDuree=1;          ;;
            --conf)     CONF_PSP_PATH="$parametre";     ;;

            -S)
                isSimulation=1;
                ;;

            --) # créer pat getopt; juste ignoré ce parametre
                ;;

            -)  # indicateur des parametres pour les librairies/collections
                ((isPSPSup++));
                ;;

            *) #default $argument,$parametre"
                if [[ -z "$argument" ]];then break;fi
                if [[ $isPSPSup -eq 0 ]]
                then
                    librairie="$argument";
                    librairiesCall[$librairiesCallIndex]="$librairie"
                    ((librairiesCallIndex++))
                elif [[ $isPSPSup -eq 1 ]] # est ce un parametre de librairie?
                then
                    addLibParams "$argument";
                fi
                ;;
        esac
    
        shift 1; #renvoie une valeur non null
        if [[ -z "$parametre" ]];then break;fi
    done
    unset librairie param
#


#######################
# LANCEMENT DES TESTS #
#######################
beginStandard

# creation de l'espace de test
mkdir -p $TEST_REP
cd "$TEST_REP"
if [[ $? -ne 0 ]];then exit 1;fi

call_tests

endStandard

if [[ $isTrapCAsk -eq 1 ]];then exit $E_CTRL_C;fi

echo '== FIN NORMAL =='
exit 0