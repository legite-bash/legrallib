#!/bin/bash
# Fichier (tuto) de test pour le langage bash

set -u
#set -eE  # same as: `set -o errexit -o errtrace`

declare -r SCRIPT_PATH=$(realpath $0);        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=$(dirname $SCRIPT_PATH);# repertoire absolu du script (pas de slash de fin)
declare -r VERSION='2021.11.29';

if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh
. $SCRIPT_REP/test-bash-include.sh

# Activation des modes de debug
#isDebug=1;
#isShowVars=1
isShowLibs=1;
declare -r TEST_REP='/dev/shm/tests'


#declare -a tbl_test=('UN' 'DEUX' 'TROIS' 'QUATRE' )
#isInArray  "${tbl_test[@]}" 'DEUX'


    function showVarsLocal() {
        fctIn;
        displayVar 'LOG_ROOT' "$LOG_ROOT"
        fctOut; return 0;
    }


#####################
## APPEL DES TESTS ##
#####################
function _tests(){
    fctIn

    #test_echos
    #cheminsRelatifs
    #test_AfficherParam;
    #test_syntaxes
    #test_array
    #test_substitutions

    #_sourceBash # in test-bash-include.sh

    fctOut;return 0;
}


#########
# ECHOS #
#########
    addLibrairie 'test_echos'
    function test_echos(){
        fctIn;

        function ecrit_une_ligne(){
        #fctIn "$*";            # maisser commenté
        echo '1ere ligne'
        #fctOut;return 0;       # maisser commenté
        }

        function ecrit_deux_lignes(){
        #fctIn "$*";            # maisser commenté
        echo '2-1ere ligne'
        echo '2-2eme ligne'
        #fctOut;return 0;       # maisser commenté
        }


        echo "-------"
        echo ========
        local text='texte a ecrire'
        echo $text;                 # retour chariot AVANT
        echo "$text";               # retour chariot AVANT
        echo $(ecrit_une_ligne)     # retour chariot AVANT
        echo "$(ecrit_une_ligne)"   # retour chariot AVANT
        echo ********
        echo $(ecrit_deux_lignes)   # les 2 lignes inlines
        echo "$(ecrit_deux_lignes)" # chaque ligne sur une ligne
        fctOut;return 0;
    }
#


#############
# LES PATHS #
#############
    addLibrairie 'cheminsRelatifs'
    function cheminsRelatifs() {
        fctIn "$*"
        displayVar '$0' "$0"
        displayVar '${BASH_SOURCE}' "${BASH_SOURCE}"
        displayVar '${BASH_SOURCE[*]}' "${BASH_SOURCE[*]}"
        displayVar '${BASH_SOURCE[0]}' "${BASH_SOURCE[0]}"
        displayVar '${FUNCNAME[*]}' "${FUNCNAME[*]}"
        displayVar '${FUNCNAME[0]}' "${FUNCNAME[0]}"
        fctOut; return 0;
    }
#


#################
## TEST PARAMS ##
#################
    addLibrairie 'test_AfficherParam'
    function test_AfficherParam(){
        fctInEcho "$*"

        function fTxt1(){
            fctInEcho "$*"

            local p1="${1:-''}";displayVar "$FUNCNAME-p1" "$p1";
            local p2="${2:-''}";displayVar "$FUNCNAME-p2" "$p2";
            local p3="${3:-''}";displayVar "$FUNCNAME-p3" "$p3";
            local p4="${4:-''}";displayVar "$FUNCNAME-p4" "$p4";
            
            fTxt2 "$p1" "$p2" "$p3" "$p4"
            fctOut; return 0;
        }

        function fTxt2(){
            fctInEcho "$*"
            local p1="${1:-''}";displayVar "$FUNCNAME-p1" "$p1";
            local p2="${2:-''}";displayVar "$FUNCNAME-p2" "$p2";
            local p3="${3:-''}";displayVar "$FUNCNAME-p3" "$p3";
            local p4="${4:-''}";displayVar "$FUNCNAME-p4" "$p4";
            fctOut; return 0;
        }

        isDebug=1;
        fTxt1 "TXT1" 'TXT A' "TXT B"
        fTxt1 "TXT1" 10 'TXT A' "TXT B"
        fTxt1 "TXT1" 10 'TXT A' 20
        fctOut;return 0;

    }
#


##################
## TEST SYNTAXE ##
##################
    addLibrairie 'test_syntaxes'
    function test_syntaxes(){
        fctIn "$*"        

        # --------- #
        argumentDeFonctions 1 2 'trois pas tout seul' "quatre accompagné"

        # --------- #
        local names=( 'Anderson da Silva' 'Wayne Gretzky' 'David Beckham' 'Long
        Name');

        test_syntaxes_ArobaseEtoile

        # --------- #
        test_syntaxe_FonctionArobaseEtoile un deux 'trois pas     tout seul' "quatre contenant un 
            retour chariot"
        fctOut;return 0;
    }


    addLibrairie 'argumentDeFonctions'
    function argumentDeFonctions(){
        fctIn "$*"
        echo 'for "'
        local _argNb=0;
        while true
        do
            ((_argNb++))
            p=${1:-''}
            if [ "$p" == '' ];then break;fi
            displayVar "$_argNb" "$1"; shift
        fctOut;return 0;
        done
    }

    addLibrairie 'test_syntaxes_ArobaseEtoile'
    function test_syntaxes_ArobaseEtoile(){
        fctIn "$*"
        function showDataArobase(){
            fctIn "$*"
            for name in ${names[@]};  do displayVar 'names[@]' "$name"; done
            fctOut;return 0;
        }
        function showDataDollarArobase(){
            fctIn "$*"
            for name in ${names[$@]};  do displayVar 'names[$@]' "$name"; done
            fctOut;return 0;
        }
        function showDataEtoile(){
            fctIn "$*"
            for name in ${names[*]};  do displayVar 'names[*]' "$name"; done
            fctOut;return 0;
        }
        function showDataDollarEtoile(){
            fctIn "$*"
            for name in ${names[$*]};  do displayVar 'names[$*]' "$name"; done
            fctOut;return 0;
        }


        echo "Quand c'est un tableau:"
        echo "Il n'y a aucune difference entre \$@ et \$*: les 2 renvoient le 1er indice"
        echo "Il n'y a aucune difference entre @ et $*: les 2 renvoient TOUS les indices"
        echo "Avec l'IFS avec espace: chaque mot devient un indice"

        echo -e "\nWith default IFS value..."
        IFS=$' \n\t'  # ' \n\t' is the default IFS value
        #IFS=$' \n\t'

        showDataArobase
        showDataEtoile
        showDataDollarArobase
        showDataDollarEtoile

        echo -e "With strict-mode IFS value..."
        IFS=$'\n\t'
        showDataArobase
        showDataEtoile
        showDataDollarArobase
        showDataDollarEtoile
        fctOut;return 0;

    }

    addLibrairie 'test_syntaxe_FonctionArobaseEtoile'
    function test_syntaxe_FonctionArobaseEtoile(){
        fctIn "$*"        
        echo "Quand ce sont les parametres d'une fonction:"
        echo "Il n'y a aucune difference entre \$@ et \$*: les 2 renvoient le 1er indice"
        echo "Il n'y a aucune difference entre  @ et  *: les 2 renvoient TOUS les indices"
        echo "Avec l'IFS avec espace: chaque mot devient un indice"

        #echo '@ est le caractere @'
        #for parametre in  @; do displayVar 'parametre[@] ' "$parametre"; done
        echo '$@ est le tableau de parametres'
        for parametre in $@; do displayVar 'parametre[$@]' "$parametre"; done
        echo ''

        #echo '* est le repertoire courant'
        #for parametre in  *; do displayVar 'parametre[*] ' "$parametre"; done
        echo '$* est le tableau de parametres'
        for parametre in $*; do displayVar 'parametre[$*]' "$parametre"; done
        fctOut;return 0;
    }
#


##############
## TEST FOR ##
##############
    # --------- #
    addLibrairie 'test_array'
    function test_array(){
        fctIn "$*"
        test_tableau_iteration
        #test_for_avec_variable
        fctOut; return 0;
    }

    addLibrairie 'test_tableau_iteration'
    function test_tableau_iteration(){
        fctIn "$*"
        titre2 'Tableau associatif'
        local books=('In Search of Lost Time' 'Don Quixote' 'Ulysses' 'The Great Gatsby')
        echo '@ ';
        for book in "${books[@]}";  do  displayVar 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}"; do  displayVar 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";  do  displayVar 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}"; do  displayVar 'Book' "$book";done

        titre2 'Tableau associatif: itaration sur key'
        declare -A livres;
        livres['livre1']='livre premier';
        livres['livre2']='livre second';
        livres['livre3']='livre troisième';
        livres['livre4']='livre 4';
        for key in "${!livres[@]}"; do
            displayVar "$key" "${livres[$key]}"
        done

        titre2 'Tableau indexé'
        declare -a books;
        books[0]='In Search of Lost Time';
        books[1]='Don Quixote';
        books[2]='Ulysses';
        books[3]='The Great Gatsby';
        echo '@ ';
        for book in "${books[@]}";  do  displayVar 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}"; do  displayVar 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";  do  displayVar 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}"; do  displayVar 'Book' "$book";done
        fctOut; return 0;
    }

    addLibrairie 'test_for_avec_variable'
    function test_for_avec_variable(){
        fctIn "$*"
        #for val in {1..${end}}  do  displayVar 'val' "$val";done
        #ne fonctionne pas
        fctOut; return 0;
    }
#


########################
## TEST SUBSTITUTIONS ##
########################
    # --------- #
    # https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
    addLibrairie 'test_substitutions'
    function test_substitutions(){
        fctIn
        test_substitution_rename
        test_lowerUpperCase
        test_substitution_deleteFirstLastCar
        fctOut; return 0;
    }

    addLibrairie 'test_substitution_rename'
    function test_substitution_rename(){
        fctIn
        local original='';
        original='fichier.jpeg';   displayVar "$original" "${original%.jpeg}.jpg"
        original=' aA bB cC';       displayVar "$original" "${original// /_}"
        original='_aAbBcC';         displayVar "$original" "${original//A/}"
        fctOut; return 0;
    }


    addLibrairie 'test_lowerUpperCase'
    function test_lowerUpperCase(){
        fctIn
        # test lower/upper case
        local result="$(test_upperCase 'Texte En Masjuscule')";
        echo $result;       # inline
        echo "$result";     # retour chariot avant d'ecrire
        echo $(test_lowerCase 'Textes En Miniscule')   # inline
        echo "$(test_lowerCase 'Textes En Miniscule')" # retour chariot avant d'ecrire
        fctOut; return 0;
    }

    addLibrairie 'test_upperCase'
   function test_upperCase(){
        fctIn
            echo "${1^^}" # = UPERCASE $1
        fctOut; return 0;
    }

    addLibrairie 'test_lowerCase'
    function test_lowerCase(){
        fctIn
            echo "${1,,}" # lower $1
        fctOut; return 0;
    }

    addLibrairie 'test_substitution_deleteFirstLastCar'
    function test_substitution_deleteFirstLastCar(){
        fctIn
        evalCmd "variable='123456789'"
        echo -n 'Suppression du   1er caractere:';displayVar '${variable#?}'   "${variable#?}"
        echo -n 'Suppression de n 1er caractere:';displayVar '${variable#???}' "${variable#???}"
        echo -n 'Suppression du   dernier caractere:';displayVar '${variable%?}'   "${variable%?}"
        echo -n 'Suppression de n dernier caractere:';displayVar '${variable%???}' "${variable%???}"
        fctOut; return 0;
    }

    addLibrairie 'test_string'
    function test_string(){
        fctIn
        variable='123456789'
        displayVar 'variable' "$variable"
        echo -n 'Nb de caractere:                     '; displayVar '${#variable}'       "${#variable}"
        echo -n 'selection apres la position:         '; displayVar '${variable:2}'      "${variable:2}"
        echo -n 'selection apres la position de n car:'; displayVar '${variable:2:5}'    "${variable:2:5}"
        fctOut; return 0;
    }
#


################
## PARAMETRES ##
################
TEMP=$(getopt \
    --options v::dVhnS- \
    --long help,version,verbose::,debug,showVars,showCollections,update,conf::,showLibs,showDuree\
,none\
    -- "$@")
    eval set -- "$TEMP"

    while true
    do
        #echo ''
        if [[ $isTrapCAsk -eq 1 ]];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"

        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                ;;
            -V|--version) echo "$VERSION"; exit 0;      ;;
            -v|--verbose) ((verbosity++));              ;;
            -d|--debug)   ((isDebug++));isShowVars=1;isShowDuree=1; ;;
            --update)   isUpdate=1;                     ;;
            --showVars)         isShowVars=1;           ;;
            --showCollections)  isShowCollections=1;    ;;
            --showLibs )        ((isShowLibs++));       ;;
            --showLibsDetail)   isShowLibs=2;           ;;
            --showDuree )       isShowDuree=1;          ;;
            --conf)     CONF_PSP_PATH="$parametre";     ;;

            -S)
                isSimulation=1;
                ;;

            --) # créer pat getopt; juste ignoré ce parametre
                ;;

            -)  # indicateur des parametres pour les librairies/collections
                ((isPSPSup++));
                ;;

            *) #default $argument,$parametre"
                if [[ -z "$argument" ]];then break;fi
                if [[ $isPSPSup -eq 0 ]]
                then
                    librairie="$argument";
                    librairiesCall[$librairiesCallIndex]="$librairie"
                    ((librairiesCallIndex++))
                elif [[ $isPSPSup -eq 1 ]] # est ce un parametre de librairie?
                then
                    addLibParams "$argument";
                fi
                ;;
        esac
    
    shift 1; #renvoie une valeur non null
    if [[ -z "$parametre" ]];then break;fi
    done
    unset librairie param
#


#######################
# LANCEMENT DES TESTS #
#######################

#displayVar 'SCRIPT_PATH' "$SCRIPT_PATH"
#displayVar 'SCRIPT_REP' "$SCRIPT_REP"
#displayVar 'SCRIPT_FILENAME' "$SCRIPT_FILENAME"
#displayVar 'SCRIPT_NAME' "$SCRIPT_NAME"
#displayVar 'SCRIPT_EXT' "$SCRIPT_EXT"
#displayVar 'LOG_PATH' "$LOG_PATH"
#displayVar '$0' "$0"
#displayVar '${BASH_SOURCE[*]}' "${BASH_SOURCE[*]}"
#displayVar '${BASH_SOURCE[0]}' "${BASH_SOURCE[0]}"

beginStandard

# creation de l'espace de test
mkdir -p "$TEST_REP" && cd "$TEST_REP"
if [ $? -ne 0 ];then exit 1;fi

_tests

endStandard

#showLibs
if [ $isTrapCAsk -eq 1 ];then exit $E_CTRL_C;fi
exit 0